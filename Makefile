# My first FLTK Makefile
CC  = $(shell fltk-config --cc)
CXX = $(shell fltk-config --cxx)
#CXX = i586-mingw32msvc-g++
#DEBUG = -g
#EXEEXT = .exe

CFLAGS   = $(shell fltk-config --cflags --ldflags)
CXXFLAGS = $(shell fltk-config --cxxflags --ldflags)

LINKFLTK = $(shell fltk-config --ldstaticflags)
LINKFLTK_GL = $(shell fltk-config --use-gl --ldstaticflags)
LINKFLTK_IMG = $(shell fltk-config --use-images --ldstaticflags)

# Possible steps to run after linking...
STRIP      = strip
POSTBUILD  = fltk-config --post # Required on OSX, does nothing on other platforms, so safe to call

TARGET = editor_h$(EXEEXT)
OBJS = helloworld.o \
	editor.o
SRCS = helloworld.cxx \
	editor.cxx
	
all: $(TARGET)
	$(CXX) -o $(TARGET) $(OBJS) $(CXXFLAGS)
	$(STRIP) $(OBJS)
	# only required on OSX, but call it anyway for portability
	$(POSTBUILD) $@

%.o: %.cxx
	$(CXX) $(CXXFLAGS) $(DEBUG) -c -lfltk -lfltk_forms -lfltk_gl -lfltk_images $<
#file1.o: file1.c file1.h  # a "plain" C file
#	$(CC) -c $< $(CCFLAGS)

#file2.o: file2.cxx file2.h file1.h  # a C++ file
#	$(CXX) -c $< $(CXXFLAGS)

# Now define how to link the final app - let's assume it needs image and OpenGL support
$(TARGET): $(OBJS)
helloworld.o: helloworld.cxx
editor.o: editor.cxx

clean: $(TARGET) $(OBJS)
	rm -f *.o 2> /dev/null
	rm -f $(TARGET) 2> /dev/null
